This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It is a form with a file input so that the user can upload an xml file.
This file is parsed and presented as a tree diagram.

## Setup

For the project to be ready, you should run:

### `npm install` 
or
### `yarn install` 

## Run App

The project should run in development mode with:

### `npm run start`
or
### `yarn start`

<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Test App
You will also see any lint errors in the console.

### `npm run test`
or
### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Build App

### `npm run build`
or
### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
