import React from 'react';
import ReactDomServer from 'react-dom/server';
import { act } from 'react-dom/test-utils';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {waitFor, screen} from '@testing-library/react'
import user from '@testing-library/user-event'
import UploadForm from './UploadForm';

configure({ adapter: new Adapter() });

describe('When initializes uploadForm component should', () => {
  let component;

  beforeEach(() => {
    component = shallow(<UploadForm />);
  });

  it('render without crashing', () => {
    expect(component).toBeTruthy();
  });

  it('have label with text upload xml file', () => {
    expect(component.find('label').text()).toEqual('Upload xml file');
  });

  it('have a file input field', () => {
    expect(component.find('input[type="file"]').length).toBeTruthy();
    expect(component.find('[data-testid="uploadFormInput"]').length).toBeTruthy();
  });

  it('have no button to be visible', () => {
    expect(component.find('button').length).toBeFalsy();
  });

});

// TODO: wanted to make more tests for functionality 
// but i got stuck when searching for tests of react hooks and dom changes
// so i didn't want to spend more time on these and i have commented my work so far

// describe('When tests uploadForm component functionality should', () => {
//   let component;
//   let resetFile = jest.fn();
//   let setUploadedFile = jest.fn();
//   const setState = jest.fn();
//   const useStateSpy = jest.spyOn(React, "useState")
//   useStateSpy.mockImplementation((init) => [init, setState]);
//   const whenStable = async () => {
//     await act(async () => {
//       await new Promise((resolve) => setTimeout(resolve, 0));
//     });
//   };

//   beforeEach(() => {
//     component = mount(<UploadForm />);
//   });

//   afterEach(() => {
//     jest.clearAllMocks();
//   });

//   it('fill file input & show remove file button & set state', () => {
//     const uploadFormInput = component.find('[data-testid="uploadFormInput"]').at(0);
//     const element = React.createElement("animal", {type: "guinea pig", name: "Sparkles"});
//     const elementXML = ReactDomServer.renderToStaticMarkup(element);
//     const file = new Blob([elementXML], {
//       type: "text/xml"
//     });
//     uploadFormInput.simulate('change', { target: { files: [new Response(file)] } });
//     const removeFileButton = component.find('[data-testid="removeFile"]');
//     expect(useStateSpy).toHaveBeenCalled();
//   });

//   it('call resetFile if remove btn is clicked', () => {
//     component.instance().setState({hasSubmittedFile: true});
//     component.find('.fileRemove').simulate('click');
//     expect(setState).toHaveBeenCalled();
//   });
// });
