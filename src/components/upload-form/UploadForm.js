import React, { useState } from 'react'
import './UploadForm.scss';

// A dumb component for showing the form with the file input.

const UploadForm = (props) => {
  const [uploadedFile, setUploadedFile] = useState({});
  const [hasSubmittedFile, setHasSubmittedFile] = useState(false);

  const onSubmit = async (e) => {
    e.preventDefault();
    setHasSubmittedFile(true);
    props.fileUploadHandle(uploadedFile);
  }

  const fileUploadHandle = (e) => {
    e.target.files[0].text().then((response) => setUploadedFile(response));
  }

  const resetFile = () => {
    props.fileUploadReset();
    setUploadedFile({});
    setHasSubmittedFile(false);
  }

  const render = () => {
    if (!hasSubmittedFile) {
      return (
        <form onSubmit={onSubmit}>
          <div className="fileUploadInput">
            <label>
              <span>Upload xml file</span>
            </label>
            <input
              accept=".xml"
              onChange={fileUploadHandle}
              type="file"
              data-testid="uploadFormInput"
            />
            {uploadedFile.length && <button
              type="submit"
              style={{ marginTop: '10px' }}>
                Submit
              </button>
            }
          </div>
        </form>
      )
    } else {
      return (
        <div>
          {props.fileIsCorrupted && 
            <div className="corrupt-file">
              File is corrupted. <br />Please remove & try uploading another file.
            </div>
          }
          <div className="fileRemove">
            <button onClick={resetFile} data-testid="removeFile">
              Remove File
            </button>
          </div>
        </div>
      );
    }
  }

  return (
    <div >
      {render()}
    </div>
  );
}

export default UploadForm;