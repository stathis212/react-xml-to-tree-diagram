import React from 'react';
import Tree from 'react-d3-tree';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Presentation from './Presentation';

configure({ adapter: new Adapter() });

describe('When initializes presentation component should', () => {

  it('render without crashing', () => {
    shallow(<Presentation />);
  });

});
