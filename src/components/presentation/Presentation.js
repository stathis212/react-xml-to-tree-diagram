import React from 'react';
import Tree from 'react-d3-tree';
import './Presentation.scss';

// A dumb component for showing the diagram tree when a file has been uploaded.

const Presentation = ({data}) => {
  const containerStyles = {
    width: '100%',
    height: '100vh',
  }

  const svgSquare = {
    shape: 'rect',
    shapeProps: {
      width: 200,
      height: 35,
      x: -10,
      y: -30,
      rx: 5,
      ry: 5,
      fill: '#70e170',
      stroke: '#70e170',
      color: 'white'
    }
  }

  const translate = {
    x: 500,
    y: 100
  }
  
  return (
    <div id="treeWrapper" style={containerStyles}>
      <Tree data={data} orientation={'horizontal'} nodeSvgShape={svgSquare} initialDepth={0} translate={translate} allowForeignObjects={true} />
    </div>
  );
}

export default Presentation;