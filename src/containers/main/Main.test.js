import React from 'react';
import Main from './Main';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('When initializes main component it should', () => {

  it('render without crashing', () => {
    shallow(<Main />);
  });

  it('render upload form but not presentation', () => {
    const wrapper = mount(<Main />);
    const uploadForm = wrapper.find('[data-testId="uploadFormComponent"]').exists();
    expect(uploadForm).toBeTruthy();
    const presentation =  wrapper.find('[data-testId="presentationComponent"]').exists();
    expect(presentation).toBeFalsy();
  });

});
