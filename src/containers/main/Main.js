import React, {useState} from 'react';
import './Main.scss';
import UploadForm from '../../components/upload-form/UploadForm';
import Presentation from '../../components/presentation/Presentation';
import XMLParser from 'react-xml-parser';

const Main = () => {
  const [results, setResult] = useState(null);
  const [fileIsCorrupted, setFileIsCorrupted] = useState(null);

  const fileUploadHandle = (file) => {
    try {
      const jsonDataFromXml = new XMLParser().parseFromString(file);
      setResult(jsonDataFromXml);
      setFileIsCorrupted(false);
    } catch (error) {
      console.log(error);
      setResult(null);
      setFileIsCorrupted(true);
    }
  }

  const fileUploadReset = () => {
    setResult(null);
    setFileIsCorrupted(false);
  }
  
  return(
    <div className="main-container">
      <UploadForm fileIsCorrupted={fileIsCorrupted} fileUploadHandle={fileUploadHandle} fileUploadReset={fileUploadReset} data-testId="uploadFormComponent"/>
      {results && <div><Presentation data={results} data-testId="presentationComponent" /></div>}
    </div>
  );
}

export default Main;
