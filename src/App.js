import React from 'react';
import './App.css';

import Main from './containers/main/Main';

function App() {
  return (
    <div className="app">
      <header className="app-header">
        <img src={process.env.PUBLIC_URL + '/img/axio_logo.png'} className="app-logo" alt="logo" />
        <h3>A fun & friendly programming challenge!</h3>
      </header>
      <div className="app-body">
        <Main />
      </div>
      <footer className="app-footer">
        <div>&#169; Footer</div>
      </footer>
    </div>
  );
}

export default App;
